#!/bin/bash

x86_64-w64-mingw32-g++ -shared -g -Wall -Iinclude/ -o payload.dll src/payload.c
x86_64-w64-mingw32-g++ -g -Wall -Iinclude/ -o injector.exe src/injector.c -lpsapi -lshell32
