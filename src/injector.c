#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <shellapi.h>

int find_processids_by_name(LPWSTR image_name, DWORD* result, DWORD limit) {
    PROCESSENTRY32W entry;
    DWORD count = 0;
    entry.dwSize = sizeof(PROCESSENTRY32W);
    

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32FirstW(snapshot, &entry) == TRUE)
    {
        do {
            if (_wcsicmp(entry.szExeFile, image_name) == 0)
            {
                result[count] = entry.th32ProcessID;
                count += 1;
            }

        } while (Process32NextW(snapshot, &entry) == TRUE && count < limit );
    }

    CloseHandle(snapshot);

    return count;
}

void list_process() {
    PROCESSENTRY32W entry;
    entry.dwSize = sizeof(PROCESSENTRY32W);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

    if (Process32FirstW(snapshot, &entry) == TRUE)
    {
        do {
            printf("%S\n", entry.szExeFile);
        } while (Process32NextW(snapshot, &entry) == TRUE);
    }

    CloseHandle(snapshot);
}

void list_process_modules(DWORD pid) {
    MODULEENTRY32W entry;
    entry.dwSize = sizeof(MODULEENTRY32W);

    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);

    if (Module32FirstW(snapshot, &entry) == TRUE) {
        do {
            printf("  %S(%p)\n", entry.szExePath, entry.modBaseAddr);
        } while (Module32NextW(snapshot, &entry) == TRUE);
    }

    CloseHandle(snapshot);
}

void inject_dll(HANDLE target, LPWSTR payload_path) {
    size_t written;
    HANDLE hMem = VirtualAllocEx(target, NULL, wcslen(payload_path)* sizeof(WCHAR), MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    HMODULE k32base = GetModuleHandle("kernel32.dll");
    FARPROC hProc = GetProcAddress(k32base, "LoadLibraryW");
    WriteProcessMemory(target, hMem, payload_path, wcslen(payload_path)*sizeof(WCHAR), &written);
    printf("%p\n", k32base);
    printf("%p\n", hProc);
    HANDLE hThread = CreateRemoteThread(target, NULL, 0, (LPTHREAD_START_ROUTINE)hProc, hMem, 0, NULL);
    WaitForSingleObject(hThread, INFINITE);
    VirtualFreeEx(target, hMem, wcslen(payload_path)*sizeof(WCHAR), MEM_DECOMMIT|MEM_RELEASE);
    CloseHandle(hThread);
}

void EnableDebugPriv()
{
    HANDLE hToken;
    LUID luid;
    TOKEN_PRIVILEGES tkp;

    OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);

    LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid);

    tkp.PrivilegeCount = 1;
    tkp.Privileges[0].Luid = luid;
    tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof(tkp), NULL, NULL);

    CloseHandle(hToken); 
}


int main() {

    DWORD pids[1024] = {0};
    int pid_count = 0;

    WCHAR tName[1024] = {0};
    WCHAR tPayload[1024] = {0};
    int do_inject = 0;

    int argc = 0;
    LPWSTR *argv;

    argv = CommandLineToArgvW(GetCommandLineW(), &argc);

    EnableDebugPriv();

    list_process();

    if (argc > 1) {
        wcscpy(tName, argv[1]);
        if (argc > 2) {
            do_inject = 1;
            if(!_wfullpath(tPayload, argv[2], 1024)) {
              do_inject = 0;
              printf("Encountered Error while expanding dll path.\n");
            }
        }
    } else {
        wcscpy(tName, L"cmd.exe");
    }

    printf("Finding Target...\n");
    pid_count = find_processids_by_name(tName, pids, 1024);

    if (pid_count == NULL) {
        printf("%S not running.\n", tName);
        return 1;
    } else {
        printf("Found %d instances of %S running\n", pid_count, tName);
    }

    for(int i = 0; i < pid_count; i++) {
        WCHAR pool[1024] = {0};
        int name_len = 0;
        HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pids[i]);
        if((name_len = GetModuleBaseNameW(proc, NULL, pool, 1023)) <= 0){
            printf("Something wrong reading pid: %lu\n", pids[i]);
        } else {
            printf("%S(%lu)\n", pool, pids[i]);
        }

        puts("Before:");
        list_process_modules(pids[i]);

        getchar();
        if (do_inject) {
            printf("Injecting '%S'\n", tPayload);
            inject_dll(proc, (LPWSTR)tPayload);
        }

        getchar();
        puts("After:");
        list_process_modules(pids[i]);

        CloseHandle(proc);
    }

}
