#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

__declspec(dllexport) BOOL APIENTRY DllMain(HMODULE, DWORD, LPVOID);

#ifdef __cplusplus
}
#endif

void random_stuff() {
  int ret = 0;
  if(!GetModuleHandle("user32.dll")) {
    LoadLibraryA("C:\\Windows\\System32\\user32.dll");
  }
  ret = MessageBoxW(NULL, L"Nothing", L"Nothing", MB_OK | MB_ICONINFORMATION | MB_SYSTEMMODAL | MB_TOPMOST);
}

__declspec(dllexport) BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason, LPVOID lpReserved) {
  if (ul_reason == DLL_PROCESS_ATTACH) {
    random_stuff();
  }
  return TRUE;
}
